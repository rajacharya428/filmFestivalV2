// let expenses = [];
// let totalAmount = 0;

// const catagorySelect = document.getElementById("catagory-select");
// const amountInput = document.getElementById("amount-input");
// const dateInput = document.getElementById("date-input");
// const addBtn = document.getElementById("add-btn");
// const expensesTableBody = document.getElementById("expeense-table-body");
// const totalAmountCell = document.getElementById("total-amount");

// addBtn.addEventListener("click", function(){
//     const catagory = catagorySelect.value;
//     const amount = Number(amountInput.value);
//     const date = dateInput.value;
//     if(catagory===""){
//         alert("please select a catagory");
//         return;
//     }
//     if( isNaN(amount) || amount <= 0) {
//         alert("please enter a valid amount");
//         return;
//     }
//     if( date === ""){
//         alert("Please enter a date")
//         return;
//     }
//     expenses.push({catagory,amount, date});
//     totalAmount += amount;
//     totalAmountCell.textContent = totalAmount;
//     const newRow = expensesTableBody.insertRow();
//     const amountCell = newRow.insertCell();
//     const dateCell = newRow.insertCell();
//     const deleteCell = newRow.insertCell();
//     const deleteBtn = document.createElement("button");


//     deleteBtn.textContent = "delete";
//     deleteBtn.classList.add("delete-btn");

//     deleteBtn.addEventListener("click" , function(){
//         expenses.splice(expenses.indexOf(expense),1);

//         totalAmount-= expense.amount;
//         totalAmountCell.textContent = totalAmount;

//         expensesTableBody.removeChild*(newRow);


//     });
//     const expense = expenses[expenses.length - 1];
//     categoryCell. textContent = expense.catagory;
//     amountCell. textContent = expense.amount;
//     dateCell. textContent = expense.date;
//     deleteCell. appendChild(deleteBtn);


// });

// for ( const expense of expenses){
//     totalAmount += expense.amount ;
//     totalAmountCell.textContent = totalAmount;
//     const newRow = expensesTableBody.insertRow()
//     const amountCell = newRow.insertCell();
//     const dateCell = newRow.insertCell();
//     const deleteCell = newRow.insertCell();
//     const deleteBtn = document.createElement("button");
    
//     deleteBtn.textContent = "delete";
//     deleteBtn.classList.add("delete-btn");

//     deleteBtn.addEventListener("click" , function(){
//         expenses.splice(expenses.indexOf(expense),1);

//         totalAmount-= expense.amount;
//         totalAmountCell.textContent = totalAmount;

//         expensesTableBody.removeChild*(newRow);


//     });
//     const expense = expenses[expenses.length - 1];
//     categoryCell. textContent = expense.catagory;
//     amountCell. textContent = expense.amount;
//     dateCell. textContent = expense.date;
//     deleteCell. appendChild(deleteBtn);


// }

let expenses = [];
let totalAmount = 0;

const categorySelect = document.getElementById("catagory-select");
const amountInput = document.getElementById("amount-input");
const dateInput = document.getElementById("date-input");
const addBtn = document.getElementById("add-btn");
const expensesTableBody = document.getElementById("expenses-table-body");
const totalAmountCell = document.getElementById("total-amount");

addBtn.addEventListener("click", function(){
    const category = categorySelect.value;
    const amount = Number(amountInput.value);
    const date = dateInput.value;

    if (category === ""){
        alert("Please select a category");
        return;
    }
    if (isNaN(amount) || amount <= 0) {
        alert("Please enter a valid amount");
        return;
    }
    if (date === ""){
        alert("Please enter a date");
        return;
    }

    expenses.push({ category, amount, date });
    totalAmount += amount;
    totalAmountCell.textContent = totalAmount;

    const newRow = expensesTableBody.insertRow(-1);
    const categoryCell = newRow.insertCell();
    const amountCell = newRow.insertCell();
    const dateCell = newRow.insertCell();
    const deleteCell = newRow.insertCell();
    const deleteBtn = document.createElement("button");

    deleteBtn.textContent = "Delete";
    deleteBtn.classList.add("delete-btn");

    deleteBtn.addEventListener("click", function() {
        const expense = expenses.find(exp => exp.category === category && exp.amount === amount && exp.date === date);
        if (expense) {
            expenses.splice(expenses.indexOf(expense), 1);
            totalAmount -= expense.amount;
            totalAmountCell.textContent = totalAmount;
            expensesTableBody.removeChild(newRow);
        }
    });

    const expense = expenses[expenses.length - 1];
    categoryCell.textContent = expense.category;
    amountCell.textContent = expense.amount;
    dateCell.textContent = expense.date;
    deleteCell.appendChild(deleteBtn);
});