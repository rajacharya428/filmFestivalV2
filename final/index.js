

const slider = document.querySelector('.slider');
const movieCards = document.querySelectorAll('.movie-card');
const prevBtn = document.querySelector('.prev-btn');
const nextBtn = document.querySelector('.next-btn');
const cardWidth = movieCards[0].offsetWidth;
const visibleCards = 4;
let currentIndex = 0;

function slideTo(index) {
  if (index < 0 || index >= movieCards.length) {
    return;
  }
  
  const maxIndex = movieCards.length - visibleCards;
  const position = Math.max(0, Math.min(index, maxIndex));
  const translateX = -position * cardWidth;
  slider.style.transform = `translateX(${translateX}px)`;
  currentIndex = position;
  updateButtonState();
}

function slideNext() {
  slideTo(currentIndex + 1);
}

function slidePrev() {
  slideTo(currentIndex - 1);
}

function updateButtonState() {
  prevBtn.disabled = currentIndex === 0;
  nextBtn.disabled = currentIndex === movieCards.length - visibleCards;
}

// Event listeners for next/previous buttons
nextBtn.addEventListener('click', slideNext);
prevBtn.addEventListener('click', slidePrev);

// Initialize button state
updateButtonState();


// Get all movie cards
const MovieCards = document.querySelectorAll(".movie-card");

// Get the modal element
const modal = document.getElementById('modal');

// Function to open the modal for a specific movie
function openModal(name, imageSrc, description, trailerLink) {
  // Update modal content with movie details
  document.getElementById('modal-name').textContent = name;
  document.getElementById('modal-image').src = imageSrc;
  document.getElementById('modal-description').textContent = description;
  document.getElementById('modal-trailer').href = trailerLink;

  // Show the modal
  modal.style.display = 'block';
}

// Function to close the modal
function closeModal() {
  modal.style.display = 'none';
}

// Loop through all movie cards and attach event listeners
MovieCards.forEach((movieCard) => {
  movieCard.addEventListener("click", () => {
    // Get movie details from the clicked card's data attributes
    const name = movieCard.dataset.name;
    const imageSrc = movieCard.dataset.imageSrc;
    const description = movieCard.dataset.description;
    const trailerLink = movieCard.dataset.trailerLink;

    // Open the modal with the extracted movie details
    openModal(name, imageSrc, description, trailerLink);
  });
});

// Get the close button element
const close = document.querySelector(".close");

// Event listener for the close button
close.addEventListener('click', closeModal);

// Event listener for clicking outside the modal to close it
window.addEventListener('click', (event) => {
  if (event.target === modal) {
    closeModal();
  }
});






const submit = document.getElementById("register-submit");

submit.addEventListener("click", (e)=>{
e.preventDefault()
var name1 = document.getElementById('name1').value;
var lastName = document.getElementById('last-name').value;
var email = document.getElementById('exampleInputEmail1').value;
var number = document.getElementById('number').value;
var checkOut = document.getElementById('check-out').value;
var body = 'name: ' + name1 +'<br/> LastName: ' + lastName + '<br/> email: ' + email + '<br/> number: ' + number 
Email.send({
  Host : "smtp.gmail.com",
  Username : "rajacharya824@gmail.com",
  Password : "wuqmyapbpszvgbfn",
  To : 'them@website.com',
  From : email,
  Subject : number,
  Body : body
}).then(
message => alert(message)
);
})

